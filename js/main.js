/*
    Get all available charts from db
*/
function load_all_charts() {
    $('.existing-charts').html('');
    $.ajax({
        type: "GET",
        dataType: "json",
        timeout:18000,
        url: "endpoints/get_all_charts",
        success: function (data) {
            
            $(data).each(function(arrIndex, arrValue) {
                var chartPayload = {chartId: arrValue.id};
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    timeout:18000,
                    data: JSON.stringify(chartPayload),
                    url: "endpoints/get_chart_detail",
                    success: function (data) {
                        var chartBox = '<div class="row">' +
                        '<div class="col-sm-6">' +
                        '    <label for="">JSON response</label>' +
                        '    <textarea name="" class="form-control" style="height:250px">' + JSON.stringify(data.chart, null, 2) + '</textarea>' +
                        '</div>' +
                        '<div class="col-sm-6">' +
                        '    <canvas id="canvas' + arrIndex + '"></canvas>' +
                        '</div>' +
                        '</div>';
                        $('.existing-charts').append(chartBox);
                        
                        var ctx = document.getElementById('canvas' + arrIndex);
                        var myChart = new Chart(ctx, data.chart);
                        
                    }
                });
            });
        }
    });
}
$( document ).ready(function() {
    load_all_charts();
});
/*
    Random color generator
*/
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
/*
  Init bootstrap color picker
*/
$(function() {
    $('.colorPicker').colorpicker({
        format: 'rgb'
    });
});
/*
    Show modal
*/
var myArray = ['January', 'February', 'March','April','May','June','July','September','October','November','December'];
$('.show-modal').click(function(){
    var rand = myArray[Math.floor(Math.random() * myArray.length)];
    
    $('#new_data_series input[name=data_series]').val(rand);
    $('#new_data_series input[name=bgcolor]').colorpicker('setValue',getRandomColor());
    $('#new_data_series input[name=bordercolor]').colorpicker('setValue',getRandomColor());
    $('#exampleModal').modal('show');
    $('.colorPicker').colorpicker({
        format: 'rgb'
    });
    return false;
});
/*
    Returns a random number between min (inclusive) and max (exclusive)
*/
function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function reindexAll() {
    $(".core-data").each(function(index) {
        var prefix = "data[" + index + "]";
        $(this).find("input").each(function() {
           this.name = this.name.replace(/data\[\d+\]/, prefix);   
        });
    });
}
/*
    Set data from modal
*/
var arr = new Array();
$('.new_data_series').click(function(){
    if ($('#new_data_series input[name=data_series]').val()!="")
    {
        var new_series = {name:$('#new_data_series input[name=data_series]').val(),bgcolor:$('#new_data_series input[name=bgcolor]').val(),bordercolor:$('#new_data_series input[name=bordercolor]').val()};
        arr.push(new_series);
        var new_series_index = $(arr).length;
        var data=$('#new_data_series').serialize();
        $('.emptyTable').remove();
        $('table tbody').append('<tr><td>' + $('#new_data_series input[name=data_series]').val() + '</td><td>' + $('#new_data_series input[name=bgcolor]').val() + '</td><td>' + $('#new_data_series input[name=bordercolor]').val() + '</td></tr>');
        $('.data-sets').append('<div class="row">' +
                                    '<div class="col-sm-3">' +
                                        '<label for="name">Data series</label>' +
                                        '<input type="text" value="' + $('#new_data_series input[name=data_series]').val() + '" name="data[0][set][_XLABEL_' + new_series_index + '][label]" class="form-control" disabled>' +
                                    '</div>' +
                                    '<div class="col-sm-3">' +
                                        '<label for="name">Value</label>' +
                                        '<input type="text" value="' + getRandomArbitrary(7,49) + '" name="data[0][set][_XLABEL_' + new_series_index + '][value]" class="form-control">' +
                                    '</div>' +
                                    '<div class="col-sm-3">' +
                                        '<label for="name">Background color</label>' +
                                        '<div class="colorPicker input-group colorpicker-component">' +
                                            '<input type="text" value="' + $('#new_data_series input[name=bgcolor]').val() + '" class="form-control" name="data[0][set][_XLABEL_' + new_series_index + '][bgcolor]" />' +
                                            '<span class="input-group-addon"><i></i></span>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-sm-3">' +
                                        '<label for="name">Border color</label>' +
                                        '<div class="colorPicker input-group colorpicker-component">' +
                                            '<input type="text" value="' + $('#new_data_series input[name=bordercolor]').val() + '" class="form-control" name="data[0][set][_XLABEL_' + new_series_index + '][bordercolor]" />' +
                                            '<span class="input-group-addon"><i></i></span>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>');
        $('.colorPicker').colorpicker('update');
        $('.core-data').show();
        $('.confirm-buttons').show();
        $('#exampleModal').modal('hide');
        reindexAll();
    }
    else {
        alert('Please set name');
    }
    return false;
});
/*
    Create new data set
*/
var myColors = ['White','Silver','Gray','Black','Navy','Blue','Cerulean','Sky','Blue','Turquoise','Green','Azure'];
$('.new-data-set').click(function(){
    $('.core-data-holder .core-data:first-child').clone().appendTo('.core-data-holder');
    
    var randColor = myColors[Math.floor(Math.random() * myColors.length)];
    
    $('.core-data-holder .core-data:last-child input.data-set-name').val(randColor);

    $('.colorPicker').colorpicker('update');
    reindexAll();
    return false;
});
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
      rv[i] = arr[i];
    return rv;
  }
/*
    Create payload
*/
$('.create-chart').click(function(){
    
    var core = JSON.parse($($('form#core-data-form')).serializeJSON().replace(/_XLABEL_/g, ""));
    
    var xLabels     = [];
    var xDatasets   = [];

    $( core.data ).each(function( index, value ) {
        xLabels.push(value.label);
    });

    $( arr ).each(function( arrIndex, arrValue ) {
        var xValue   = [];
        var xBackgroundColor = [];
        var xBorderColor = [];
        $( core.data ).each(function( index, value ) {
            xValue.push(parseInt(value.set[arrIndex+1].value));
            xBackgroundColor.push(value.set[arrIndex+1].bgcolor);
            xBorderColor.push(value.set[arrIndex+1].bordercolor);
        });
       
        xDatasets.push({ label: arrValue.name,data: xValue, backgroundColor:xBackgroundColor, borderColor: xBorderColor, borderWidth: 1});
    });
    
    var resultPayload = new Object();
        resultPayload.type = core.type;
        resultPayload.chartName = core.chartname;
        resultPayload.data = { labels: xLabels, datasets: xDatasets };
        resultPayload.options = {scales:{yAxes:[{ticks:{beginAtZero: true}}]}};
        
    $.ajax({
        type: "POST",
        dataType: "json",
        timeout:18000,
        data: JSON.stringify(resultPayload),
        url: "endpoints/new_chart",
        success: function (data) {
            load_all_charts();
            alert('done');
        }
    });
    return false;
});