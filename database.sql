-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Čtv 24. srp 2017, 22:44
-- Verze serveru: 10.1.9-MariaDB
-- Verze PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `madeo`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `ma_charts`
--

CREATE TABLE `ma_charts` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `options` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `ma_charts`
--

INSERT INTO `ma_charts` (`id`, `name`, `type`, `options`) VALUES
(1, '', 'bar', '{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}');

-- --------------------------------------------------------

--
-- Struktura tabulky `ma_charts_data`
--

CREATE TABLE `ma_charts_data` (
  `id` int(10) NOT NULL,
  `chart_id` int(10) NOT NULL,
  `series_id` int(10) NOT NULL,
  `value` int(10) NOT NULL,
  `label` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `bgcolor` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `bordercolor` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `borderwidth` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `ma_charts_data`
--

INSERT INTO `ma_charts_data` (`id`, `chart_id`, `series_id`, `value`, `label`, `bgcolor`, `bordercolor`, `borderwidth`) VALUES
(1, 1, 1, 35, 'Red', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(2, 1, 1, 90, 'Blue', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(3, 1, 1, 2, 'Yellow', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(4, 1, 1, 10, 'Green', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(5, 1, 1, 77, 'Purple', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(6, 1, 1, 21, 'Orange', 'rgb(19,108,132)', 'rgb(51,255,7)', 1),
(7, 1, 2, 43, 'Red', 'rgb(244,235,136)', 'rgb(119,210,119)', 1),
(8, 1, 2, 23, 'Blue', 'rgb(244,235,136)', 'rgb(119,210,119)', 1),
(9, 1, 2, 89, 'Yellow', 'rgb(244,235,136)', 'rgb(119,210,119)', 1),
(10, 1, 2, 90, 'Green', 'rgb(244,235,136)', 'rgb(119,210,119)', 1),
(11, 1, 2, 49, 'Purple', 'rgb(244,235,136)', 'rgb(119,210,119)', 1),
(12, 1, 2, 14, 'Orange', 'rgb(244,235,136)', 'rgb(119,210,119)', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `ma_charts_series`
--

CREATE TABLE `ma_charts_series` (
  `id` int(10) NOT NULL,
  `chart_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `ma_charts_series`
--

INSERT INTO `ma_charts_series` (`id`, `chart_id`, `name`) VALUES
(1, 1, '# of Votes'),
(2, 1, '# of Attendance');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `ma_charts`
--
ALTER TABLE `ma_charts`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `ma_charts_data`
--
ALTER TABLE `ma_charts_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chart_id` (`chart_id`);

--
-- Klíče pro tabulku `ma_charts_series`
--
ALTER TABLE `ma_charts_series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chart_id` (`chart_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `ma_charts`
--
ALTER TABLE `ma_charts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `ma_charts_data`
--
ALTER TABLE `ma_charts_data`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pro tabulku `ma_charts_series`
--
ALTER TABLE `ma_charts_series`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
