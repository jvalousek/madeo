<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chart_lib
{
  	protected 	$ci;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->model('chart_model');
    }
    /*
        Create new chart
    */
	public function create_new_chart($data) {
        
        if (!isset($data->data->labels) || !isset($data->data->datasets))
        {
            $error['success']=false;
            $error['message']="Invalid payload";
            return $error;
        }
        else {
            if (empty($data->data->labels) || empty($data->data->datasets))
            {
                $error['success']=false;
                $error['message']="Empty data labels or datasets";
                return $error;
            }
        }
        /*
            Create new chart
        */
        $req = array(
            'name'      => (isset($data->chartName) ? $data->chartName : ""),
            'type'      => $data->type,
            'options'   => json_encode($data->options)
        );
        $chartId = $this->ci->chart_model->create_new_chart($req);
        
        /*
            Create data series
        */
        foreach($data->data->datasets as $key=>$val)
        {
            $req = array(
                'chart_id'  => $chartId,
                'name'      => $val->label
            );
            $serieId = $this->ci->chart_model->create_new_series($req);

            /*
                Create data rows
            */
            foreach($data->data->labels as $xKey=>$xVal)
            {
                $dValue=array('data','backgroundColor','borderColor');
                foreach($dValue as $dVal)
                {
                    if (count($data->data->labels)!=count($data->data->datasets[$key]->{$dVal}))
                    {
                        //Remove created rows above in case of any error
                        $this->ci->chart_model->remove_chart($chartId);
                        $this->ci->chart_model->remove_serie_by_chart_id($chartId);

                        $error['success']=false;
                        $error['message']="Data labels and datasets for " . $dVal . " missmatch";
                        return $error;
                    }   
                }
                $req=array(
                    'chart_id'      => $chartId,
                    'series_id'     => $serieId,
                    'label'         => $data->data->labels[$xKey],
                    'value'         => $data->data->datasets[$key]->data[$xKey],
                    'bgcolor'       => $data->data->datasets[$key]->backgroundColor[$xKey],
                    'bordercolor'   => $data->data->datasets[$key]->borderColor[$xKey],
                    'borderwidth'   => $data->data->datasets[$key]->borderWidth
                );
                $this->ci->chart_model->create_new_chart_data($req);
            }
        }
        $res['success']=true;
        $res['message']="Chart is successfully created.";
        $res['chartId']=$chartId;
        $res['origPayload']=$data;
        return $res;
    }
    /*
        Return all available charts
    */
    public function get_all() {
        return $this->ci->chart_model->get_all();
    }
    /*
        Build and return selected chart
    */
    public function get_chart_detail($data) {
        
        if (!isset($data->chartId))
        {
            $error['success']=false;
            $error['message']="Missing parameter chartId";
            return $error;
        }
        if (!$chartDetail = $this->ci->chart_model->get_chart_detail($data->chartId))
        {
            $error['success']=false;
            $error['message']="Chart with this ID does not exist";
            return $error;
        }
        $res['chart']['type']       = $chartDetail->type;
        $res['chart']['chartName']  = $chartDetail->name;

        $chartData      = $this->ci->chart_model->get_data_by_chart_id($chartDetail->id);
        $exLabels       = false;
        $sIt            = false;
        foreach($chartSeries    = $this->ci->chart_model->get_series_by_chart_id($chartDetail->id) as $key=>$val)
        {
            foreach($chartData as $chData)
            {
                /* TODO - labels */
                if (($exLabels==false || $exLabels==$chData->series_id) && $sIt==false)
                {
                    $exLabels=$chData->series_id;
                    $res['chart']['data']['labels'][] = $chData->label;
                }
                
                if ($val->id==$chData->series_id)
                {
                    $res['chart']['data']['datasets'][$key]['label'] = $val->name;
                    $res['chart']['data']['datasets'][$key]['data'][]  = (int)$chData->value;
                    $res['chart']['data']['datasets'][$key]['backgroundColor'][]  = $chData->bgcolor;
                    $res['chart']['data']['datasets'][$key]['borderColor'][]  = $chData->bordercolor;
                    $res['chart']['data']['datasets'][$key]['borderWidth']  = $chData->borderwidth;
                }
            }
            $sIt=true;
        }
        $res['chart']['options']=json_decode($chartDetail->options);
       

        $res['success']=true;
        return $res;
    }
}