<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Rendering graphs</title>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css"/>
	<style>
		html {
			margin:10px
		}
	</style>
</head>
<body>
<!-- Modal start -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<form action="#" id="new_data_series">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Add new data series</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<label>Name</label>
			<input type="text" name="data_series" class="form-control" placeholder="# of Votes" required="true">

			<label for="name">Default background color</label>
			<div class="colorPicker input-group colorpicker-component">
				<input type="text" name="bgcolor" value="#00AABB" class="form-control" />
				<span class="input-group-addon"><i></i></span>
			</div>

			<label for="name">Default border color</label>
			<div class="colorPicker input-group colorpicker-component">
				<input type="text" name="bordercolor" value="#00AABB" class="form-control" />
				<span class="input-group-addon"><i></i></span>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary new_data_series">Save changes</button>
		</div>
		</div>
	</form>
  </div>
</div>
<!-- Modal end -->

<div id="container">
	<form action="" id="core-data-form">
		<input type="hidden" name="type" value="bar">
		<h2>Create new chart</h2>
		<div class="row">
			<div class="col-sm-6">
				<label for="name">Chart name</label>
				<input type="text" value="" name="chartname" class="form-control">
			</div>
		</div>
		<div class="dataSets">
			<div class="row">
				<div class="col-sm-6">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Data series name</th>
							<th>Default bg color</th>
							<th>Default border color</th>
						</tr>
						</thead>
						<tbody>
						<tr class="emptyTable">
							<td colspan="3" style="text-align:center">Please set new data series</td>
						</tr>
						</tbody>
					</table>
					<a href="#" class="btn btn-primary show-modal">Add new data series to your chart</a>
					
				</div>
				<div class="core-data-holder col-sm-6">
					<div class="core-data" style="background:#F0F0F0;display:none">
						<div class="row">
							<div class="col-sm-12">
								<label for="name">Label</label>
								<input type="text" value="Red" name="data[0][label]" class="form-control data-set-name" style="background:rgba(255, 250, 164, 1)">
							</div>
						</div>
						<div class="data-sets"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-offset-6 confirm-buttons" style="display:none">
					<a href="#" class="btn btn-primary new-data-set">New data set</a>
					<a href="#" class="btn btn-success create-chart">Create new chart</a>
				</div>
			</div>
		</div>
	</form>
		<h2>Existing charts</h2>
		<div class="existing-charts"></div>
		
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/chart.js/dist/Chart.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/jquery-serialize-object/dist/jquery.serialize-object.min.js"></script>
<script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>