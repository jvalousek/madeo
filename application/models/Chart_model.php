<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chart_model extends CI_Model {

	public function create_new_chart($req) {
        $this->db->insert('ma_charts', $req);
        return $this->db->insert_id();
	}
    public function create_new_series($req) {
        $this->db->insert('ma_charts_series', $req);
        return $this->db->insert_id();
    }
    public function create_new_chart_data($req) {
        $this->db->insert('ma_charts_data', $req);
        return $this->db->insert_id();
    }
    public function remove_chart($id){
        $this->db->delete('ma_charts', array('id' => $id));
    }
    public function remove_serie_by_chart_id($id) {
        $this->db->delete('ma_charts_series', array('chart_id' => $id));
    }
    public function get_all() {
        return $this->db->select('id,name')
                        ->order_by('id', 'desc')                
                        ->get('ma_charts')
                        ->result();
    }
    public function get_chart_detail($id) {
        return $this->db->where('id',$id)
                        ->get('ma_charts')
                        ->row(); 
    }
    public function get_series_by_chart_id($id) {
        return $this->db->where('chart_id',$id)
                        ->get('ma_charts_series')
                        ->result(); 
    }
    public function get_data_by_chart_id($id) {
        return $this->db->where('chart_id',$id)
                    ->get('ma_charts_data')
                    ->result(); 
    }
}
