<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Endpoints extends CI_Controller {
    function __construct()
	{
		parent::__construct();
		$this->load->library('chart_lib');
	}
	public function new_chart()
	{
        if ($this->input->raw_input_stream)
        {
            if ($input = json_decode($this->input->raw_input_stream))
            {
                $data = $this->chart_lib->create_new_chart($input);
            }
            else {
                $data['success'] = false;
                $data['message'] = 'Wrong payload. Payload must be valid JSON (RFC 4627)';
            }
        }
        else {
            $data['success'] = false;
            $data['message'] = 'Missing payload';
        }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(($data['success'] == true) ? '200' : '500')
                    ->set_output(json_encode($data,JSON_PRETTY_PRINT));
    }
    public function get_all_charts() {
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($this->chart_lib->get_all(),JSON_PRETTY_PRINT)); 
    }
    public function get_chart_detail() {
        if ($this->input->raw_input_stream)
        {
            if ($input = json_decode($this->input->raw_input_stream))
            {
                $data = $this->chart_lib->get_chart_detail($input);
            }
            else {
                $data['success'] = false;
                $data['message'] = 'Wrong payload. Payload must be valid JSON (RFC 4627)';
            }
        }
        else {
            $data['success'] = false;
            $data['message'] = 'Missing payload';
        }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(($data['success'] == true) ? '200' : '500')
                    ->set_output(json_encode($data,JSON_PRETTY_PRINT));
    }
}
